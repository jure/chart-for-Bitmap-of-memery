#include "cbasictype.h"
CBasicType::CBasicType()
{
	m_ClassFlag = "CBasicType";
	m_ForeColor = 0xFF000000;
	m_BackColor = 0xFFFFFFFF;
	m_Enable  = true;
}

void CBasicType::Enable(bool en)
{
	m_Enable = en;
}

void CBasicType::SetColor(uint color)
{
	m_ForeColor = color;
}

void CBasicType::SetBackColor(uint color)
{
	m_BackColor = color;
}

uint& CBasicType::GetColor()
{
	return m_ForeColor;
}

uint& CBasicType::GetBackColor()
{
	return m_BackColor;
}

void CBasicType::SetPos(CPointF pos)
{
	m_MaxArea.TopLeft() = pos;
}

void CBasicType::SetSize(CPointF size)
{
	m_MaxArea.Size() = size;
}

CPointF& CBasicType::GetPos()
{
	return m_MaxArea.TopLeft();
}

CPointF& CBasicType::GetSize()
{
	return m_MaxArea.Size();
}
