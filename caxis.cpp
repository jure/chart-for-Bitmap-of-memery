#include "caxis.h"
CAxis::CAxis()
{
	m_ClassFlag = "CAxis";
	m_Auto = true;
	m_DecPoint = 2;
	m_pGridColor = NULL;
	m_pGrid = NULL;
	m_EnabelGrid = true;
}

void CAxis::EnableGrid(bool en)
{
	m_EnabelGrid = en;
}

CAxisTop::CAxisTop()
{
	m_ClassFlag = "CAxisTop";
	m_Enable = false;
	m_EnabelGrid = false;
}

CAxisRight::CAxisRight()
{
	m_ClassFlag = "CAxisRight";
	m_Enable = false;
	m_EnabelGrid = false;
}

bool CAxis::Adjust()
{	
	return m_Enable;
}

bool CAxis::OnDraw(CGriphics* pG)
{
	if(!CBasicType::OnDraw(pG))
		return false;

	m_Unit.OnDraw(pG);
	/// Draw Axis Begin

	char szAxis[100];
	char szTemp[20] = {'\0'};
	sprintf(szTemp, "%%.%df", m_DecPoint);
	int m_number = 10;
	for(int idx=0; idx<=m_number; idx++)
	{		
		sprintf(szAxis, szTemp, GetActualByIdx(idx));
		DrawAxis(pG, szAxis, GetPosByIdx(idx));
		DrawGrid(pG, GetPosByIdx(idx));
		if(m_number == 1) break;
	}

	return false;
}

float   CAxis::GetActualByIdx(int nIdx)
{
	return 0.0f; // m_BeginValueEx+nIdx*m_minStep;
}

CPointF CAxisHorizontal::GetPosByIdx(int idx)	//	left right
{
	CPointF pt(0,0);
	// pt.Y = m_Board.GetBottom() - (m_BeginViewEx - m_Board.Y) - minStepView*idx;

	return pt;
}

CPointF CAxisVertial::GetPosByIdx(int idx)
{
	CPointF pt(0,0);
	// pt.X = m_BeginViewEx+minStepView*idx;
	return pt;
}

CPointF CAxisLeft::GetPosByIdx(int idx)
{
	CPointF pt(0,0);
	pt.X = 0;
	// pt += CAxisHorizontal::GetPosByIdx(idx);
	return pt;
}

CPointF CAxisRight::GetPosByIdx(int idx)
{
	CPointF pt(0,0);
	pt.X = 0;
	// pt += CAxisHorizontal::GetPosByIdx(idx);
	return pt;
}

CPointF CAxisTop::GetPosByIdx(int idx)
{
	CPointF pt(0,0);
	pt.Y = 0;
	// pt += CAxisVertial::GetPosByIdx(idx);
	return pt;
}

CPointF CAxisBottom::GetPosByIdx(int idx)
{
	CPointF pt(0,0);
	pt.Y = 0;
	// pt += CAxisVertial::GetPosByIdx(idx);
	return pt;
}

void CAxisLeft::DrawAxis(CGriphics* pG, char* pszAxis, CPointF pos)
{
//	CLabel::DrawLabel(pszAxis, pos, CLabel::emDirStr.LEFT | CLabel::emDirStr.CENTER_V);	
}

void CAxisRight::DrawAxis(CGriphics* pG, char* pszAxis, CPointF pos)
{	
//	CLabel::DrawLabel(pszAxis, pos, CLabel::emDirStr.RIGTH | CLabel::emDirStr.CENTER_V);
}

void CAxisTop::DrawAxis(CGriphics* pG, char* pszAxis, CPointF pos)
{
//	CLabel::DrawLabel(pszAxis, pos, CLabel::emDirStr.CENTER_H | CLabel::emDirStr.UP);
}

void CAxisBottom::DrawAxis(CGriphics* pG, char* pszAxis, CPointF pos)
{	
//	CLabel::DrawLabel(pszAxis, pos, CLabel::emDirStr.CENTER_H | CLabel::emDirStr.DOWN);		
}

void CAxisHorizontal::DrawGrid(CGriphics* pG, CPointF pos)
{
	CPointF ptBegin = pos;
	CPointF ptEnd(0,pos.Y);
	DrawBar(pG, pos, CPointF(pos.X-5, pos.Y));
}
void CAxisVertial::DrawGrid(CGriphics* pG, CPointF pos)
{
	CPointF ptBegin = pos;
	CPointF ptEnd(0,pos.Y);
	DrawBar(pG, pos,CPointF(pos.X-5, pos.Y));
}

void CAxis::DrawBar(CGriphics* pG, CPointF begin, CPointF end)
{
}