#ifndef _AXIS_H
#define _AXIS_H

#include "clabel.h"
class CAxis : public CBasicType
{
public:
	CAxis();
	virtual bool OnDraw(CGriphics* pG);
	void EnableGrid(bool en);
	
protected:
	virtual void    DrawAxis(CGriphics*, char* pszAxis, CPointF pos) = 0;
	virtual CPointF GetPosByIdx(int nIdx) = 0;
	virtual void DrawGrid(CGriphics*, CPointF pos) = 0;
	float   GetActualByIdx(int nIdx);
	void    DrawBar(CGriphics*, CPointF begin, CPointF end);
	bool	Adjust();          // return EnableDraw?

protected:
	bool	m_Auto;
	CLabel  m_Unit;	
	CPointF m_Actual;   	// actualValue
	CPointF m_Original; 	// OriginalValue
	CPointF m_ActualSize;	// actualView
	int 	m_DecPoint;		// Number of Decimal point 
public:
	CRectF* m_pGrid;
	uint*  m_pGridColor;	// == 0x00xxxx not Draw
	bool 	m_EnabelGrid;
};

class CAxisHorizontal : public CAxis
{
public:
protected:	
	virtual CPointF GetPosByIdx(int idx);
	void DrawGrid(CGriphics*, CPointF pos);
};

class CAxisVertial : public CAxis
{
public:
protected:	
	virtual CPointF GetPosByIdx(int idx);
	void DrawGrid(CGriphics*, CPointF pos);

};

class CAxisLeft : public CAxisHorizontal
{
public:
	CAxisLeft(){m_ClassFlag = "CAxisLeft";}
protected:
	virtual void    DrawAxis(CGriphics*, char* pszAxis, CPointF pos);
	CPointF GetPosByIdx(int idx);
	
};

class CAxisRight : public CAxisHorizontal
{
public:
	CAxisRight();
protected:
	virtual void    DrawAxis(CGriphics*, char* pszAxis, CPointF pos);
	CPointF GetPosByIdx(int idx);
};

class CAxisTop : public CAxisVertial
{
public:
	CAxisTop();
protected:
	virtual void    DrawAxis(CGriphics*, char* pszAxis, CPointF pos);
	CPointF GetPosByIdx(int idx);
};

class CAxisBottom : public CAxisVertial
{
public:
	CAxisBottom(){m_ClassFlag = "CAxisBottom";}
protected:
	virtual void    DrawAxis(CGriphics*, char* pszAxis, CPointF pos);
	CPointF GetPosByIdx(int idx);
};

#endif