#ifndef _LINE_H
#define _LINE_H

#include "caxis.h"
class CLine : public CBasicType
{
public:
	CLine();
	~CLine();
	virtual bool OnDraw(CGriphics*);
	CLine*  Copy();
	PointF* GetPtr();
	bool	AttachAxis();
	int		Size();
	void	SetSize(int len);
	int		GetLength();
	void	SetLine(PointF* pts, int len);
	RectF 	GetRectShow();
	bool	Adjust();

	CLine& operator+(PointF& point)
	{
		CPointF* ptr = (CPointF*)GetPtr();
		int len = GetLength();
		while(len--)
		{
			*ptr++ += point;
		}
		return *this;
	}
	
	CLine& operator-(PointF& point)
	{
		return *this+(CPointF(-point.X, -point.Y));
	}

	CLine& operator*(PointF& point)
	{
		CPointF* ptr = (CPointF*)GetPtr();
		int len = GetLength();
		while(len--)
		{
			*ptr++ *= point;
		}
		return *this;
	} 

public:
	CRectF * m_pGraphRect;
	CAxisHorizontal * m_pAxisH;
	CAxisVertial * m_pAxisV;
private:
	CRectF   m_rectShow;
	PointF * points;
	int		 length;
};

#endif
