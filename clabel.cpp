#include "clabel.h"

CLabel::CLabel()
{
	this->m_ClassFlag = "CLabel";
}

bool CLabel::Adjust()
{
	return true;
}

bool CLabel::OnDraw(CGriphics* pG)
{
	if(!CBasicType::OnDraw(pG))
		return false;
	return true;
}

void CLabel::SetName(char* szName)
{
	m_Value += szName;
}