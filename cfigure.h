#ifndef _FIGURE_H
#define _FIGURE_H

#include "cboard.h"
class CFigure : public CBoard
{
public:
	CFigure();
	~CFigure();
	virtual bool OnDraw(CGriphics*);
	bool CreateBoard(int nRow, int nColumn = 1);
	CBoard * GetBoard(int nBoardIdx); // for 0 to nRow * nColumn - 1
	bool Adjust();	
private:
	std::list<CBoard*> m_Boards;	
	int m_nBoardRow;
	int m_nBoardColumn;
};

#endif