#include "cgriphics.h"
#include <math.h>

class CGriphicsBitmap: public CGriphics
{
public:
		CGriphicsBitmap(){};
public:

	CGriphicsBitmap(CBitmap* pBitmap) : pG(pBitmap){};
	~CGriphicsBitmap(){	pG = NULL;}

	void DrawLines(PointF* pts, int len, uint color)
	{
		int width, height;

		for(int idx=1; idx<len; idx++)
		{
			width = (int)pts[idx].X - (int)pts[idx-1].X ? (int)pts[idx].X - (int)pts[idx-1].X : 1;
			height = (int)pts[idx].Y-(int)pts[idx-1].Y ? (int)pts[idx].Y-(int)pts[idx-1].Y : 1;
			pG->DrawLine((uint)pts[idx-1].X, (uint)pts[idx-1].Y, width, height, (uint)color);
		}
	};

	void DrawRect(RectF& rect, uint color)
	{
		pG->DrawRect((uint)rect.X, (uint)rect.Y, (uint)rect.Width, (uint)rect.Height, (uint)color);
	};

	void FillRect(RectF& rect, uint color)
	{
		pG->FillRect((uint)rect.X, (uint)rect.Y, (uint)rect.Width, (uint)rect.Height, (uint)color);
	};

protected:
	CBitmap * pG;
};


////////////// CGriphics ///////////////////////////

CGriphics* CGriphics::FromBitmap(CBitmap* pBitmap)
{
	if(pBitmap == NULL)
		return NULL;

	if(pBitmap->GetBitCount() == 32 || pBitmap->GetBitCount() == 24)
		return new CGriphicsBitmap(pBitmap);
	return NULL;
}

RectF CGriphics::GetTranslateRate(RectF& rectView, RectF& rectValue)
{
	RectF rectRate;
	rectRate.X = rectView.X - rectValue.X;
	rectRate.Y = rectView.Y - rectValue.Y;

	rectRate.Width = rectValue.Width==0.0f? 1.0f : rectView.Width / rectValue.Width;
	rectRate.Height = rectValue.Height==0.0f? 1.0f : rectView.Height / rectValue.Height;

	return rectRate;
}

PointF CGriphics::GetTranslateRate(PointF& pointView, PointF& pointValue)
{
	CPointF pointRate(0.0f, 1.0f);
	if(pointValue.Y == 0.0f)
		return pointRate;

	pointRate.X = pointView.X - pointValue.X;
	pointRate.Y = pointView.Y / pointValue.Y;
	return pointRate;
}

void CGriphics::TranslateTransform(float* datas, int size, const float trs)
{
	if(datas == NULL || size < 1) return;

	while(size --)
	{
		*datas++ += trs;
	}
}

void CGriphics::TranslateTransform(PointF* line, int size, const PointF trsPoint)
{
	if(line == NULL || size < 1) return;

	CPointF *pLine = (CPointF*)line;

	while(size--)
	{
		*pLine++ += trsPoint;
	}
}

void CGriphics::ScaleTransform(float* datas, int size, const float scale)
{
	if(datas == NULL || size < 1) return;

	while(size --)
	{
		*datas++ *= scale;
	}
}

void CGriphics::ScaleTransform(PointF* line, int size, const PointF scalePoint)
{
	if(line == NULL || size < 1) return;

	CPointF *pLine = (CPointF*)line;
	while(size--)
	{
		*pLine++ *= scalePoint;
	}
}
