#include "cline.h"
CLine::CLine()
{
	m_pAxisH = NULL;
	m_pAxisV = NULL;
	m_pGraphRect = NULL;

	points = NULL;
	length = 0;
	m_ClassFlag = "CLine";
}

CLine::~CLine()
{
	SetSize(0);
}

bool CLine::Adjust()
{
	if(m_pGraphRect == NULL || !m_Enable || GetLength() < 1)
		return false;
	
	*(RectF*)&m_rectShow = GetRectShow();		
	return true;
}

bool CLine::OnDraw(CGriphics* pG)
{
	if(!CBasicType::OnDraw(pG))
		return false;

	CRectF rView = *m_pGraphRect;
	CRectF rShow = m_rectShow;

	CLine *pLine = this->Copy();
	*pLine = ((*pLine-rShow.TopLeft()) * (rView/rShow) ) + rView.TopLeft();
	pG->DrawLines(pLine->GetPtr(), this->Size(), this->GetColor());

	delete pLine;
	pLine = NULL;  
	return true;
}

CLine* CLine::Copy()
{
	CLine * newLine = new CLine();
	newLine->SetLine(this->GetPtr(), Size());
	return newLine;
}

void CLine::SetLine(PointF* pts, int len)
{
	SetSize(len);
	memcpy(GetPtr(), pts, len*sizeof(PointF));
}

PointF* CLine::GetPtr()
{
	return points;
}

void CLine::SetSize(int len)
{
	if(len != Size())
	{
		delete points;
		points = NULL;
		length = 0;
		if(len > 0)
		{
			points = new CPointF[len];
			memset(points, 0, len * sizeof(PointF));
			length = len;
		}
	}
}

RectF CLine::GetRectShow()
{
	CRectF  rect;
	CPointF *pts = (CPointF*)GetPtr();

	if(pts == NULL || GetLength() < 1)
		return rect;

	rect.TopLeft() = rect.Size() = *pts++; 
	for(int idx=1; idx<GetLength(); idx++, pts++)
	{
		if(pts->X >= rect.Size().X)			// max_X
			rect.Size().X = pts->X;
		else if(pts->X < rect.TopLeft().X)	// min_X
			rect.TopLeft().X = pts->X;

		if(pts->Y >= rect.Size().Y)			// max_Y
			rect.Size().Y = pts->Y;
		else if(pts->Y < rect.TopLeft().Y)	// min_Y
			rect.TopLeft().Y = pts->Y;		
	}
	rect.Size() += -rect.TopLeft();

	return rect;
}

inline int CLine::Size()
{
	return length;
}

inline int	CLine::GetLength()
{
	return length;
}
