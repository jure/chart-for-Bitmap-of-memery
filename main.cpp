#include "cfigure.h"
#include "bitmap.hpp"
#include <math.h>

void ChartV3_Figures()
{
	CFigure figure;
	figure.SetPos(CPointF(0.0f, 0.0f));
	figure.SetSize(CPointF(400.0f, 300.0f));
	figure.SetBackColor(0);

	figure.m_Title.SetName((char*)"figure");
	figure.m_Title.SetColor(0xFF00FF00);

	figure.CreateBoard(4, 1);

	CLine *pLine = {NULL};
	pLine = new CLine();

	pLine->SetSize(800);
	pLine->SetColor(0);

	CPointF* pts = (CPointF*)pLine->GetPtr();
	for(int idx=0; idx<pLine->GetLength(); idx++)
		pts[idx] = CPointF(1.0f*idx, 250.0f+500.0f*sin(3.1415927/40*idx));

	figure.GetBoard(0)->SetBackColor(0x880000FF);
	figure.GetBoard(1)->SetBackColor(0x88FF00FF);
	figure.GetBoard(2)->SetBackColor(0x8800FFFF);
	figure.GetBoard(3)->SetBackColor(0x88FF0000);

	figure.GetBoard(0)->AddLine(pLine);
	figure.GetBoard(1)->AddLine(pLine);
	figure.GetBoard(2)->AddLine(pLine);
	figure.GetBoard(3)->AddLine(pLine);

	CBitmap* pBitmap = CBitmap::CreateBitmap32((uint)figure.GetSize().X, (uint)figure.GetSize().Y);

	CGriphics * pG = CGriphics::FromBitmap(pBitmap);
	figure.OnDraw(pG);

	delete pLine; pLine = NULL;
	delete pG; 	  pG = NULL;

	pBitmap->SaveFile((char*)"bim.bmp");
	pBitmap->SaveFile((char*)"src/bim.bmp");
	delete pBitmap;
}

void ChartV3_Signle()
{
	CFigure figure;
	figure.SetPos(CPointF(0.0f, 0.0f));
	figure.SetSize(CPointF(400.0f, 300.0f));
	figure.SetBackColor(0xFF00FF00);

	figure.m_Title.SetName((char*)"figure");
	figure.m_Title.SetColor(0xFF00FF00);

	CLine *pLine = NULL;
	pLine = new CLine();
	pLine->SetSize(1000);
	pLine->SetColor(0xFFFF0000);
	
	CPointF* pts = (CPointF*)pLine->GetPtr();
	for(int idx=0; idx<pLine->GetLength(); idx++)
		pts[idx] = CPointF(1.0f*idx, 250.0f+500.0f*sin(3.1415927/40*idx));


	figure.AddLine(pLine);

	CBitmap32 bitmap(figure.GetSize().X, figure.GetSize().Y);
	CGriphics * pG = CGriphics::FromBitmap(&bitmap);
	figure.OnDraw(pG);

	delete pLine; pLine = NULL;
	delete pG; 	  pG = NULL;

	bitmap.SaveFile((char*)"bim.bmp");
}

int main()
{
//#define WAY1 1
#if WAY1
	ChartV3_Figures();
#else
	ChartV3_Signle();
#endif
	return 0;
}
