#include "cfigure.h"

CFigure::CFigure()
: m_nBoardRow(0), m_nBoardColumn(0)
{
	this->m_ClassFlag = "CFigure";
}

CFigure::~CFigure()
{
	m_Boards.clear();
}

bool CFigure::Adjust()
{	
	if(!CBoard::Adjust())
		return false;

	CPointF size(this->GetSize().X / m_nBoardColumn, this->GetSize().Y / m_nBoardRow);
	CPointF pos = this->GetPos();

	std::list<CBoard*>::iterator it = m_Boards.begin();
	for (int i = 0; i < m_nBoardRow; ++i)
	{
		for (int j = 0; j < m_nBoardColumn; ++j)
		{
			(*it)->SetPos(pos);		// m_Boards[i*m_nBoardColumn+j].SetPos(pos);
			(*it)->SetSize(size);		// m_Boards[i*m_nBoardColumn+j].SetSize(size);
			++it;
			pos.X += size.X;
		}
		pos.X = this->GetPos().X;
		pos.Y += size.Y;
	}

	return true;
}

bool CFigure::OnDraw(CGriphics* pG)
{
	if(!CBasicType::OnDraw(pG))
		return false;

	CBoard::OnDraw(pG);
	// DrawBackground
	std::list<CBoard*>::iterator it;
	for(it = m_Boards.begin(); it != m_Boards.end(); ++it)
	{			
		(*it)->OnDraw(pG);
	}

	return true;
} 

bool CFigure::CreateBoard(int nRow, int nColumn)
{
	if(nRow<=0 || nColumn<=0)
		return false;

	for(int idx=0; idx<nRow*nColumn; idx++)
		m_Boards.push_back(new CBoard());

	m_nBoardRow = nRow;
	m_nBoardColumn = nColumn;

	EnabelAxis() = false;
	return true;
}

CBoard* CFigure::GetBoard(int nIdx)
{
	std::list<CBoard*>::iterator it = m_Boards.begin();
	while(nIdx--) ++it;

	return it != m_Boards.end() ? *it : NULL;
}