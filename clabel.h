#ifndef _LABEL_H
#define _LABEL_H

#include "cbasictype.h"
class CLabel : public CBasicType
{
public:
	enum emDirStr
	{
		LEFT = 1, RIGHT = 2, UP = 4, DOWN = 8, CENTER_H = 16, CENTER_V = 32
	};
	CLabel();
	bool Adjust();
	virtual bool OnDraw(CGriphics*);
	void SetName(char* szName);
protected:
	std::string m_Value;
};

class CLabelLeft : public CLabel
{
public:
	CLabelLeft();
	~CLabelLeft();
	
};

class CLabelReight : public CLabel
{
public:
	CLabelReight();
	~CLabelReight();
	
};

class CLabelTop : public CLabel
{
public:
	CLabelTop();
	~CLabelTop();
	
};

class CLabelButtom : public CLabel
{
public:
	CLabelButtom();
	~CLabelButtom();
	
};


#endif