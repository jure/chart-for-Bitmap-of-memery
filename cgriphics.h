#ifndef _GRIPHICS_H
#define _GRIPHICS_H

#include <iostream>
#include "bitmap.hpp"
#define OUT(msg) std::cout<<#msg<<"\n"
typedef  unsigned int uint;

typedef struct _POINTF
{
	float X;
	float Y;
}PointF, SizeF;

class CPointF : public PointF
{
public:
	CPointF(){X=0; Y=0;}
	CPointF(float x, float y){X=x; Y=y;};

	void operator*=(PointF pt)
	{
		X *= pt.X; 
		Y *= pt.Y;
	//	printf("(%f, %f) * (%f, %f)\n", X, Y, pt.X, pt.Y);
	};

	void operator+=(PointF pt)
	{
		X += pt.X;
		Y += pt.Y;		
	//	printf("(%f, %f) + (%f, %f)\n", X, Y, pt.X, pt.Y);
	};

	CPointF operator-()
	{
		return CPointF(-X, -Y);
	}
};

typedef struct _RECTF
{
	float X;
    float Y;
    float Width;
    float Height;
}RectF;

class CRectF : public RectF
{
public:
	CRectF()
	{
		X = Y = Width = Height = 0.0f;
	};

	float GetBottom() const
    {
        return Y+Height;
    };

    CPointF& TopLeft() const
    {
    	return *((CPointF*)this);
    };

    CPointF& Size() const
    {
    	return *((CPointF*)this+1);
    };

    void operator*=(const CPointF& pt)
	{
		X *= pt.X;
		Y *= pt.Y;
	};

	void operator+=(const CPointF& pt)
	{
		X += pt.X;
		Y += pt.Y;
	};

	SizeF operator/(const CRectF& rect)
	{
		SizeF size;
		size.X = rect.Width==0.0f ? 1.0f : this->Width  / rect.Width;
		size.Y = rect.Height==0.0f? 1.0f : this->Height / rect.Height;
		return size;
	};
};

class CGriphics
{
public:
	virtual RectF  GetTranslateRate(RectF& view,  RectF& value);
	virtual PointF GetTranslateRate(PointF& view, PointF& value);

	virtual void TranslateTransform(float x, float y){OUT(TranslateTransform_x_y);};
	virtual void TranslateTransform(float* datas, int size, const float trs);
	virtual void TranslateTransform(PointF* pts,  int size, const PointF trs);

	virtual void ScaleTransform(float x, float y){OUT(ScaleTransform_x_y);};
	virtual void ScaleTransform(float* datas, int size, const float scale);
	virtual void ScaleTransform(PointF* line, int size, const PointF scale);

	virtual void ResetTransform(){OUT(ResetTransform);};

	virtual void DrawLines(PointF*, int len, uint color) = 0;

	virtual void DrawRect(RectF& rect, uint color) = 0;

	virtual void FillRect(RectF& rect, uint color) = 0;

//	virtual void DrawString(char* str, uint color);
/*
	virtual int  SetClip(RectF&);
	virtual void ResetClip();
*/
	static CGriphics* FromBitmap(CBitmap*); // CBitmap*
};

class CGriphicsPlus : public CGriphics
{
public:
	CGriphicsPlus(){};
	CRectF getTranslateRate(CRectF&, CRectF&){OUT(getTranslateRate); return CRectF();};

	void TranslateTransform(float x, float y){OUT(TranslateTransform);};

	void ScaleTransform(float x, float y){OUT(ScaleTransform);};

	void TranslateTransform(PointF* lines, int len, PointF& origin){OUT(TranslateTransform);};

	void ScaleTransform(PointF* lines, int len, PointF& origin){OUT(ScaleTransform);};

	void ResetTransform(){OUT(ResetTransform);};

	void DrawLines(PointF*, int len, uint color) {OUT(DrawLines);};

	void DrawRect(RectF& rect, uint color){OUT(DrawRect);};

	void FillRect(RectF& rect, uint color){OUT(FillRect);};
};

#endif
