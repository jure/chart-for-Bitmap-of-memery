#ifndef _BOARD_H
#define _BOARD_H

#include "cbasictype.h"
#include "clabel.h"
#include "cline.h"
#include <list>
class CBoard : public CBasicType
{
public:
	CBoard();

	virtual bool OnDraw(CGriphics*);
	void AddLine(CLine * pLine);
	bool& EnabelAxis(){return m_EnabelAxis;}
	bool	Adjust();

public:
	CLabel m_Title; 	
protected:
	CRectF m_GraphRect;
	std::list<CLabel*> m_pLabels;	
	std::list<CLine*>  m_pLines;
private:
	CAxisLeft   m_AxisLeft;
	CAxisRight  m_AxisRight; 
	CAxisTop    m_AxisTop;
	CAxisBottom m_AxisBottom;
	bool 		m_EnabelAxis;
};

#endif
