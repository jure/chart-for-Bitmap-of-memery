#ifndef _BASICTYPE_H
#define _BASICTYPE_H
#include "cgriphics.h"
#include <string>
#include <iostream>

class CBasicType
{
public:
	CBasicType();
	void	Enable(bool en);
	uint&	GetColor();
	void	SetColor(uint color);
	uint&	GetBackColor();
	void	SetBackColor(uint color);
	void    SetPos(CPointF pos);
	void 	SetSize(CPointF size);
	CPointF& GetPos();
	CPointF& GetSize();	
	virtual bool Adjust() = 0;
	virtual bool OnDraw(CGriphics*)
	{		
		std::cout<< m_ClassFlag << '\n';
		return Adjust();
	};
	 	
protected:
	std::string  m_ClassFlag;
	uint  m_ForeColor;		// firstColor
	uint  m_BackColor;
	bool   m_Enable;
	CRectF m_MaxArea;	
};


#endif
