#include "cboard.h"
#include <algorithm>
CBoard::CBoard()
{
	m_ClassFlag = "CBoard";

	m_AxisLeft.m_pGrid 	= &m_GraphRect; 
	m_AxisRight.m_pGrid = &m_GraphRect; 
	m_AxisTop.m_pGrid 	= &m_GraphRect; 
	m_AxisBottom.m_pGrid = &m_GraphRect; 

	m_EnabelAxis = true;
}

void CBoard::AddLine(CLine * pLine)
{
	if(pLine != NULL)
	{
		std::list<CLine*>::iterator it = std::find(m_pLines.begin(), m_pLines.end(), pLine);
		if ( it == m_pLines.end() )
		{				
			m_pLines.push_back(pLine);			
		}
	}
}

bool CBoard::Adjust()
{
	m_GraphRect.TopLeft() = GetPos();
	m_GraphRect.Size() = GetSize();

	std::list<CLine*>::iterator it;
	for(it = m_pLines.begin(); it != m_pLines.end(); ++it)
	{
		(*it)->m_pAxisH = &m_AxisLeft;
		(*it)->m_pAxisV = &m_AxisBottom;
		(*it)->m_pGraphRect = &m_GraphRect;
	}

	return true;
}

bool CBoard::OnDraw(CGriphics* pG)
{
	if(!CBasicType::OnDraw(pG))
		return false;

	pG->DrawRect(this->m_MaxArea, 0);	// DrawBackground
	pG->FillRect(this->m_MaxArea, this->GetBackColor());
	// DrawForeground
	m_Title.OnDraw(pG);

	if(EnabelAxis() == true)
	{
		m_AxisLeft.OnDraw(pG);
		m_AxisRight.OnDraw(pG);
		m_AxisTop.OnDraw(pG);
		m_AxisBottom.OnDraw(pG);
	}
	
	std::list<CLine*>::iterator it;
	for(it = m_pLines.begin(); it != m_pLines.end(); ++it)
	{
		(*it)->OnDraw(pG);		
	}

	std::list<CLabel*>::iterator itl;
	for(itl = m_pLabels.begin(); itl != m_pLabels.end(); ++itl)
	{			
		(*itl)->OnDraw(pG);
	}

	return true;
} 